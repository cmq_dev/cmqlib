//cmqlib.i
%module cmqlib
%include "std_vector.i"
%{
#include "euopt.h"
#include "amopt.h"
#include "volmodel.h"
#include "pricer.h"
#include "barriersmile.h"
#include "timeseries.h"
%}
%include "std_string.i"
%include "std_vector.i"
%include "euopt.h"
%include "amopt.h"
%include "volmodel.h"
%include "pricer.h"
%include "barriersmile.h"
%include "timeseries.h"

namespace std {
   %template(IntVector) vector<int>;
   %template(DblVector) vector<double>;
   %template(StringVector) vector<string>;
   %template(ConstCharVector) vector<const char*>;
}
